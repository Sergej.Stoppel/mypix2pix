######################################################################################
# A version of the pix2pix network that translates images from one domain to the other
# The network consists of one encoder, one decoder and one discriminator

# Paper information, related work and other code versions are available under:
# https://phillipi.github.io/pix2pix/
# The data is taken from https://www.kaggle.com/vikramtiwari/pix2pix-dataset/download
######################################################################################


###################
# Load dependencies
###################
from random import random

from numpy import load
from numpy import zeros
from numpy import ones
from numpy import asarray
from numpy import add

from keras.layers import BatchNormalization
from keras.layers import UpSampling2D #For deconvolution upsampling
from keras.layers import Conv2D
from keras.layers import Conv2DTranspose
from keras.layers import LeakyReLU
from keras.layers import Activation
from keras.layers import Concatenate

from keras.initializers import RandomNormal

from keras.models import Input
from keras.models import Model

from keras.optimizers import Adam



##########################
# Define the discriminator
##########################

# As stated by the authors:
# The 70  70 discriminator architecture is:
# C64-C128-C256-C512
# After the last layer, a convolution is applied to map to
# a 1-dimensional output, followed by a Sigmoid function.
# As an exception to the above notation, BatchNorm is not
# applied to the first C64 layer. All ReLUs are leaky, with
# slope 0.2.

# All other discriminators follow the same basic architecture,
# with depth varied to modify the receptive field size:
# 1  1 discriminator:
# C64-C128 (note, in this special case, all convolutions are
# 1  1 spatial filters)
# 16  16 discriminator:
# C64-C128
# 286  286 discriminator:
# C64-C128-C256-C512-C512-C512

def define_discriminator(image_shape):

    # Random weight initialization
    # Weights were initialized from a 
    # Gaussian distribution with mean 0 and 
    # standard deviation 0.02.
    init = RandomNormal(mean=0.0, stddev=0.02)

    # Source image input
    img_domain_A = Input(shape=image_shape)
    img_domain_B = Input(shape=image_shape)

    # Concatenate image and conditioning image by channels to produce input
    # I first forgot this, and then there is nothing to discriminate!!!
    combined_imgs = Concatenate(axis=-1)([img_domain_A, img_domain_B])

    #################################################
    # Extracts from the paper:
    # C64-C128-C256-C512-C512-C512
    # All ReLUs are leaky, with slope 0.2.
    # BatchNorm is not applied to the first C64 layer
    # All convolutions are 4x4 spatial filters applied with stride 2.
    #################################################
	# C64
    dis = Conv2D(64, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(combined_imgs)
    dis = LeakyReLU(alpha=0.2)(dis)

    # Batch Normalization is applied from here on
	# C128
    # I can't find any more information in the batch normalization in the paper
    # So I assume that the authors use the standard values of 0.99 momemntum for moving mean and moving variance
    dis = Conv2D(128, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(dis)
    dis = BatchNormalization()(dis)
    dis = LeakyReLU(alpha=0.2)(dis)

    # C256
    dis = Conv2D(256, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(dis)
    dis = BatchNormalization()(dis)
    dis = LeakyReLU(alpha=0.2)(dis)

    # C512
    dis = Conv2D(512, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(dis)
    dis = BatchNormalization()(dis)
    dis = LeakyReLU(alpha=0.2)(dis)

    # C512
    dis = Conv2D(512, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(dis)
    dis = BatchNormalization()(dis)
    dis = LeakyReLU(alpha=0.2)(dis)

    # After the last layer, a convolution is applied to map to
    # a 1-dimensional output, followed by a Sigmoid function.
    dis = Conv2D(1, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(dis)
    return_patch = Activation('sigmoid')(dis)

    # Define model
    model = Model([img_domain_A,img_domain_B], return_patch)

	# compile model
    # We use minibatch SGD and apply the Adam solver, with a learning rate (step size) of 0:0002, and momentum parameters 1 = 0:5, 2 = 0:999
    # More info to optimizers under: https://ruder.io/optimizing-gradient-descent/index.html#adam
    model.compile(loss='mse', optimizer=Adam(lr=0.0002, beta_1=0.5, beta_2=0.999), loss_weights=[0.5])
    return model


####################
# Define the decoder
####################

###################################################################
# Notes from the authors:
# encoder:
# C64-C128-C256-C512-C512-C512-C512-C512
# decoder:
# CD512-CD512-CD512-C512-C256-C128-C64
# After the last layer in the decoder, a convolution is applied
# to map to the number of output channels (3 in general,
# except in colorization, where it is 2), followed by a Tanh
# function. As an exception to the above notation, Batch-
# Norm is not applied to the first C64 layer in the encoder.
# All ReLUs in the encoder are leaky, with slope 0.2, while
# ReLUs in the decoder are not leaky.
# The U-Net architecture is identical except with skip connections
# between each layer i in the encoder and layer n􀀀i
# in the decoder, where n is the total number of layers. The
# skip connections concatenate activations from layer i to
# layer n 􀀀 i. This changes the number of channels in the
# decoder:
# U-Net decoder:
# CD512-CD1024-CD1024-C1024-C1024-C512
# -C256-C128
##################################################################
# As the authors write the U-net architecture provides superior 
# results. Therefore we will be creating a u-net with skip connections between
# mirrored layers in the encoder and decoder stacks


# More notes from the paper:
# Let Ck denote a Convolution-BatchNorm-ReLU layer
# with k filters. CDk denotes a Convolution-BatchNorm-
# Dropout-ReLU layer with a dropout rate of 50%. All convolutions
# are 4x4 spatial filters applied with stride 2. Convolutions
# in the encoder, and in the discriminator, downsample
# by a factor of 2, whereas in the decoder they upsample by a
# factor of 2.

#
def define_encoder_decoder(image_shape):
    # Random weight initialization
    # Weights were initialized from a 
    # Gaussian distribution with mean 0 and 
    # standard deviation 0.02.
    init = RandomNormal(mean=0.0, stddev=0.02)

    # Source image input
    input_img = Input(shape=image_shape)

    # Connecting the layers is done through concatenate function
    # Resources for U-net: https://lmb.informatik.uni-freiburg.de/people/ronneber/u-net/
    # That means I should first generate the encoder, (each layer needs to be identifyable).
    # Then, generate the decoder and connect to the previously generated encoder layers.
    # The code could also be shortened by a function call to generate the layers, but I keep it
    # extended for readabilty.
    # Encoder: C64-C128-C256-C512-C512-C512-C512-C512 
    # C64
    enc1 = Conv2D(64, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(input_img)
    enc1 = LeakyReLU(alpha=0.2)(enc1)

    # Batch Normalization is applied from here on
	# C128
    # I can't find any more information in the batch normalization in the paper
    # So I assume that the authors use the standard values of 0.99 momemntum for moving mean and moving variance
    enc2 = Conv2D(128, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(enc1)
    enc2 = BatchNormalization()(enc2)
    enc2 = LeakyReLU(alpha=0.2)(enc2)

    # C256
    enc3 = Conv2D(256, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(enc2)
    enc3 = BatchNormalization()(enc3)
    enc3 = LeakyReLU(alpha=0.2)(enc3)

    # C512 (1/5)
    enc4 = Conv2D(512, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(enc3)
    enc4 = BatchNormalization()(enc4)
    enc4 = LeakyReLU(alpha=0.2)(enc4)

    # C512 (2/5)
    enc5 = Conv2D(512, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(enc4)
    enc5 = BatchNormalization()(enc5)
    enc5 = LeakyReLU(alpha=0.2)(enc5)
     
    # C512 (3/5)
    enc6 = Conv2D(512, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(enc5)
    enc6 = BatchNormalization()(enc6)
    enc6 = LeakyReLU(alpha=0.2)(enc6)

    # C512 (4/5)
    enc7 = Conv2D(512, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(enc6)
    enc7 = BatchNormalization()(enc7)
    enc7 = LeakyReLU(alpha=0.2)(enc7)

    # C512 (5/5)
    enc8 = Conv2D(512, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(enc7)
    enc8 = BatchNormalization()(enc8)
    enc8 = LeakyReLU(alpha=0.2)(enc8)

    # Nothing more is needed for the encoder.

    ####################
    # Define the decoder
    ####################

    # The decoder has skip connections from layer i to n. I am not sure why the authors define the decoder as
    # CD512-CD1024-CD1024-C1024-C1024-C512-C256-C128 in the case of a U-net. This might be wrong but here I use
    # a mirrored representation of the encoder architecture
    # Decoder: CD512-CD512-CD512-C512-C256-C128-C64 

    # Start enumeration of the decoder from the back for easier matching with the encoder
    # The convolutions in the encoder downsample the images by the factor of 2. In the decoder we 
    # need to upscale the images explicitly by 2.
    # CD512 (1/4)
    dec7 = UpSampling2D(size=2)(enc8)
    dec7 = Conv2D(512, (4,4), strides=1, padding='same', activation='relu')(dec7)
    dec7 = Concatenate()([dec7, enc7]) 

    # Batch normalization starts here
    # CD512 (2/4)
    dec6 = UpSampling2D(size=2)(dec7)
    dec6 = Conv2D(512, (4,4), strides=1, padding='same', activation='relu')(dec6)
    dec6 = Concatenate()([dec6, enc6])
    dec6 = BatchNormalization()(dec6)

    # CD512 (3/4)
    dec5 = UpSampling2D(size=2)(dec6)
    dec5 = Conv2D(512, (4,4), strides=1, padding='same', activation='relu')(dec5)
    dec5 = Concatenate()([dec5, enc5])
    dec5 = BatchNormalization()(dec5)

    # CD512 (4/4)
    dec4 = UpSampling2D(size=2)(dec5)
    dec4 = Conv2D(512, (4,4), strides=1, padding='same', activation='relu')(dec4)
    dec4 = Concatenate()([dec4, enc4])
    dec4 = BatchNormalization()(dec4)

    # C256
    dec3 = UpSampling2D(size=2)(dec4)
    dec3 = Conv2D(256, (4,4), strides=1, padding='same', activation='relu')(dec3)
    dec3 = Concatenate()([dec3, enc3])
    dec3 = BatchNormalization()(dec3)

    # C128
    dec2 = UpSampling2D(size=2)(dec3)
    dec2 = Conv2D(128, (4,4), strides=1, padding='same', activation='relu')(dec2)
    dec2 = Concatenate()([dec2, enc2])
    dec2 = BatchNormalization()(dec2)

    # C64
    dec1 = UpSampling2D(size=2)(dec2)
    dec1 = Conv2D(64, (4,4), strides=1, padding='same', activation='relu')(dec1)

    # Notes from the paper:
    # After the last layer in the decoder, a convolution is applied
    # to map to the number of output channels (3 in general,
    # except in colorization, where it is 2), followed by a Tanh
    # function. 
    # As I understand from https://keras.io/layers/convolutional/
    # Conv2DTranspose is the function for this.
    # Conv2DTranspose transposes from something that has the shape of the output of some 
    # convolution to something that has the shape of its input while maintaining a 
    # connectivity pattern that is compatible with said convolution.
    #                            # of channels
    last_layer = Conv2DTranspose(3, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(dec1)
    result_img = Activation('tanh')(last_layer)

    # Define the model
    # The optimisation of the encoder decoder has to be combined with the discriminator model
    model = Model(input_img, result_img)
    return model

    
##################################################
# Set up the combined model for generator training
##################################################
def define_combined_model(ecoder_decoder, discriminator, image_shape):
    input_img = Input(shape=image_shape)

    generator_output = ecoder_decoder(input_img)
	# Get the sourse and generated image evaluated by the discriminator
    discriminator_output = discriminator([input_img, generator_output])
    
    # The discriminator is not trainable when the generator is trained
    discriminator.trainable = False

    #Set up the model
    model = Model(input_img, [discriminator_output, generator_output])

    # compile model I have not found the details how to set up the combined model int the paper,
    # Here I have looked it up under https://github.com/eriklindernoren/Keras-GAN/tree/master/pix2pix

    model.compile(loss=['mse', 'mae'], loss_weights=[1, 100], optimizer=Adam(lr=0.0002, beta_1=0.5, beta_2=0.999))
    return model

    
#####################
# Define the training
#####################
# Notes from the paper:
# All networks were trained from scratch. Weights were
# initialized from a Gaussian distribution with mean 0 and
# standard deviation 0.02.
# Training details for edge to shoes:
# 50k training images from UT Zappos50K
# dataset [61] trained for 15 epochs, batch size 4. Data were
# split into train and test randomly.
def train(discriminator, encoder_decoder, combined_model, epochs, dataA, dataB):

    patch_shape = discriminator.output_shape[1]
    
    # Begin the training
    # 1. Load real examples
    # 2. Generate a fake example
    # 3. Train the discriminator on a fake and real image
    # 4. Train the encoder_decoder generator

    # For trainging set up I have looked at: https://www.pyimagesearch.com/2018/12/24/how-to-use-keras-fit-and-fit_generator-a-hands-on-tutorial/ 
    # and: https://keras.io/models/model/
    for i in range(epochs):
        # Go through the images one by one (TODO: I think the paper used a bigger batch size)
        for batch in range(0,len(dataA)):
            # Load examples
            real_img_A, real_img_B = dataA[batch,:], dataB[batch,:]

            # Reshape the image to a 4D tensor, this is the expected input for the encoder_decoder
            resh_real_img_A = real_img_A.reshape(1, real_img_A.shape[0], real_img_A.shape[1], real_img_A.shape[2])
            resh_real_img_B = real_img_B.reshape(1, real_img_B.shape[0], real_img_B.shape[1], real_img_B.shape[2])

            # Generate a "real" label 
            real_labeles = ones((1, patch_shape, patch_shape, 1))
            
            # Generate fake images
            fake_img_B = encoder_decoder.predict(resh_real_img_A)
            fake_labels = zeros((1, patch_shape, patch_shape, 1))

            #Train the discriminator
            discriminator_real_loss = discriminator.train_on_batch([resh_real_img_A, resh_real_img_B], real_labeles)
            discriminator_fake_loss = discriminator.train_on_batch([resh_real_img_A, fake_img_B], fake_labels)

            # Train the encoder_decoder generator
            # train_on_batch input parameters: (training data, target data, more setting)
            generator_loss = combined_model.train_on_batch(resh_real_img_A, [real_labeles, resh_real_img_B])
            
            # Summarize the progress
            print('>%d, %d' % (i * epochs + batch, epochs*len(dataA)))



################
# Execution code
################
# Test if it worked
# Load and plot the saved dataset
from numpy import load
from matplotlib import pyplot
# Load the dataset
data = load('edges2shoes.npz')

dataA, dataB = data['arr_0'], data['arr_1']
print('Loaded: ', dataA.shape, dataB.shape)
# Plot domainA images
n_samples = 3
#for i in range(n_samples):
#	pyplot.subplot(2, n_samples, 1 + i)
#	pyplot.axis('off')
#	pyplot.imshow(dataA[i].astype('uint8'))
# Plot domainB images
#for i in range(n_samples):
#	pyplot.subplot(2, n_samples, 1 + n_samples + i)
#	pyplot.axis('off')
#	pyplot.imshow(dataB[i].astype('uint8'))
#pyplot.show()


# Scale from [0,255] to [-1,1]
# (TODO) Maybe I should save the data in the correct format right away...
dataA = (dataA / 127.5) - 1.0
dataB = (dataB / 127.5) - 1.0

# Define input shape based on the loaded dataset
image_shape = dataA.shape[1:]

# Initialize the discriminator
discriminator = define_discriminator(image_shape)

# Initialize the decoder_encoder
encoder_decoder = define_encoder_decoder(image_shape)

# Initilialize the combined model
combined_model = define_combined_model(encoder_decoder, discriminator, image_shape)

# For the edges data set the authors have used 15 epochs
epochs = 15
# Train models
train(discriminator, encoder_decoder, combined_model, epochs, dataA, dataB)

# Save the model
filename = 'edge2shoes_trained.h5'
encoder_decoder.save(filename)
